#import os
#import time
import random
from math import *
import pygame
import pdb; pdb.set_trace()
from _global import *


############
##  INIT  ##
############

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
transSurface = pygame.Surface((WIDTH, HEIGHT), pygame.SRCALPHA)
pygame.display.set_caption("Planetary Orbit Insertion")
font = pygame.font.Font('freesansbold.ttf', 32)

starField = []
bodies =    {
                0:Body([WIDTH/2, HEIGHT/2], [0, 0], 10, 10000000, 0, True, BRICKRED),
                1:Body([WIDTH/4, HEIGHT/2], [0, 0.45], 5, 1000000, 0, False, DARKGREY),
            }

clickDown = False
recentBodyID = 0
DebugAccel = {}
deathQueue = []
decelPower = 1
score = 0

# Text
pwrBarTxt = "Deceleration \nBurn Power"

scoreTxt = "Satelites\nin orbit:\n"
#################
##  Main Loop  ##
#################

def main():
    run = True
    clock = pygame.time.Clock()
    global pause
    global clickDown
    global deathQueue
    global decelPower
    global DEBUG
    global recentBodyID

    while run:
        clock.tick(FPS)

        for i in deathQueue:
            bodies.pop(i)
            deathQueue.remove(i)

        if pause == False:
            updateParams()
            decelBurnCheck()
            updateScore()
        
        checkDeath()
        redraw() # Redraws screen
        pygame.display.update()
        # Grabs all the current pygame events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                print ("Exiting... All hail the void!")
                run = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                x,y = pygame.mouse.get_pos()
                recentBodyID = int(len(bodies))
                bodies[recentBodyID] = Craft([x, y], [0, 0.3], 2, 10, 0, True, BLUE)
                clickDown = True

            if event.type == pygame.MOUSEBUTTONUP:
                clickDown = False
                bodies[recentBodyID].static = False
                xVel = (bodies[recentBodyID].pos[0]- pygame.mouse.get_pos()[0]) * FIRE_POWER_CONSTANT
                yVel = (bodies[recentBodyID].pos[1] - pygame.mouse.get_pos()[1]) * FIRE_POWER_CONSTANT
                bodies[recentBodyID].vel = [xVel, yVel]


            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    pause ^=True
                if event.key == pygame.K_UP:
                    if decelPower != 10:
                        decelPower += 1
                if event.key == pygame.K_DOWN:
                    if decelPower!=0:
                        decelPower -= 1
                if event.key == pygame.K_q:
                    DEBUG ^= True
                

def createBG():
    # Fill background
    screen.fill(BLACK)

    for star in range(starNum):
        star_X = random.randrange(0, WIDTH)
        star_Y = random.randrange(0, HEIGHT)
        star_Lum = random.randrange(star_Lum_Min, star_Lum_Max)
        star_Speed = random.randrange(star_Speed_Min, star_Speed_Max)
        star_BlnkDir = bool(random.getrandbits(1))
        starField.append(
            Star(star, star_X, star_Y, star_Lum, star_Speed, star_BlnkDir))

def initSOI():
    for bID in bodies:
        if isinstance(bodies[bID], Body):
            v = sqrt(bodies[bID].vel[0]**2+bodies[bID].vel[1]**2)
            r = getBDist(bodies[bodies[bID].parentID], bodies[bID])
            bodies[bID].calcRSOI(
                v, r, bodies[bodies[bID].parentID].mass, bodies[bID].mass)

def animateBG():
    for star in starField:
        star_Colour = (star.lum, star.lum, star.lum)
        pygame.draw.circle(screen, star_Colour, (star.x, star.y), 1)
        newNumArr = bounceNum(star.lum, star.blnkSpeed,
                             star_Lum_Max, star_Lum_Min, star.blnkDir)
        star.lum = newNumArr[0]
        star.blnkDir = newNumArr[1]

def updateParent(bID):
    parent = bodies[bID].parentID
    for pID in bodies:
        if pID != bID and isinstance(bodies[pID], Body):
            d = getBDist(bodies[pID], bodies[bID])
            if d < bodies[pID].rSOI:
                parent = pID
    bodies[bID].parentID = parent


def decelBurnCheck():
    for bID in bodies:
        if isinstance(bodies[bID], Craft):
            if bodies[bID].burnComplete == False:
                if bodies[bID].velBest <= sqrt(bodies[bID].vel[0]**2 + bodies[bID].vel[1]**2):
                    bodies[bID].velBest = sqrt(bodies[bID].vel[0]**2 + bodies[bID].vel[1]**2)
                else:
                    print("deceleration: " + str(bID))
                    bodies[bID].vel[0] += bodies[bID].vel[0]*(-decelPower*0.1)
                    bodies[bID].vel[1] += bodies[bID].vel[1]*(-decelPower*0.1)

                    bodies[bID].update()
                    bodies[bID].burnComplete = True


def updateParams():
    for bID in bodies:

        if isinstance(bodies[bID], Craft):
            updateParent(bID)
        if bodies[bID].static:
            continue

        pID = bodies[bID].parentID
        tInterval = 1/FPS
        gravity = (GRAVITY*bodies[pID].mass) / (getBDist(bodies[pID], bodies[bID])**2)

        # this line determines the angle between the body and the parent with respect to the x axis
        theta = atan2(bodies[pID].pos[1]-bodies[bID].pos[1], bodies[pID].pos[0]-bodies[bID].pos[0])

        gAccel = [gravity*cos(theta), gravity*sin(theta)] # this grabs the x and y acceleration components gravitational acceleration

        DebugAccel[bID] = [gAccel[0], gAccel[1]]

        bodies[bID].vel[0] += gAccel[0]*tInterval # Calculates the change in velocity using the equation: V = V_0 + at
        bodies[bID].vel[1] += gAccel[1]*tInterval # and applies it to the body

        bodies[bID].update()

def checkDeath():
    global deathQueue
    kill = False
    for bID in bodies:
        kill = False
        if isinstance(bodies[bID], Craft):
            if getBDist(bodies[bodies[bID].parentID], bodies[bID]) <= bodies[bodies[bID].parentID].radius:
                kill = True
            
            if bodies[bID].static == False:
                if bodies[bID].pos[0] < 0:
                    kill = True
                if bodies[bID].pos[0] > WIDTH:
                    kill = True
                if bodies[bID].pos[1] < 0:
                    kill = True
                if bodies[bID].pos[1] > HEIGHT:
                    kill = True
            
        if kill:
            if bID not in deathQueue:
                deathQueue.append(bID)

def updateScore():
    global score
    score = -2
    for bID in bodies:
        if bodies[bID].burnComplete == True:
            score += 1
            print(score)


def redraw():
    global clickDown
    global decelPower
    global DEBUG
    global score
    global recentBodyID
    screen.fill(BLACK)
    transSurface.fill((0, 0, 0, 0))

    animateBG()

    blit_text(screen, pwrBarTxt, (20, 20), font, WHITE)
    blit_text(screen, scoreTxt + str(score), (20, HEIGHT-125), font, WHITE)

    pygame.draw.rect(screen, WHITE, (50, 110, 120, 50))
    pygame.draw.rect(screen,BLACK,(55,115,110,40))
    pygame.draw.rect(screen, WHITE, (60, 120, decelPower*10, 30))
    

    for bID in bodies:
        pygame.draw.circle(
            screen, bodies[bID].colour, bodies[bID].pos, bodies[bID].radius)
        
        if clickDown:
            pygame.draw.line(screen, RED, bodies[recentBodyID].pos, pygame.mouse.get_pos(), 3)
        
        if DEBUG:
            if isinstance(bodies[bID], Body):
                if bodies[bID].static == False:
                    pygame.draw.line(
                        screen, WHITE, bodies[bID].pos, (bodies[bID].pos[0]+bodies[bID].vel[0]*10, bodies[bID].pos[1]+bodies[bID].vel[1]*10))
                    pygame.draw.line(
                        screen, RED, bodies[bID].pos, (bodies[bID].pos[0]+DebugAccel[bID][0]*10, bodies[bID].pos[1]+DebugAccel[bID][1]*10))
                    pygame.draw.circle(transSurface, (bodies[bID].colour[0], bodies[bID].colour[1], bodies[bID].colour[2], 100),
                                       bodies[bID].pos, bodies[bID].rSOI)
            elif isinstance(bodies[bID], Craft):
                if bodies[bID].static == False:
                    pygame.draw.line(
                        screen, WHITE, bodies[bID].pos, (bodies[bID].pos[0]+bodies[bID].vel[0]*10, bodies[bID].pos[1]+bodies[bID].vel[1]*10))
                    pygame.draw.line(
                        screen, RED, bodies[bID].pos, (bodies[bID].pos[0]+DebugAccel[bID][0]*10, bodies[bID].pos[1]+DebugAccel[bID][1]*10))
                    pygame.draw.line(screen, BLUE, bodies[bID].pos, bodies[bodies[bID].parentID].pos)
    
    screen.blit(transSurface, (0, 0))


def blit_text(surface, text, pos, font, color=pygame.Color('black')):
    # 2D array where each row is a list of words.
    words = [word.split(' ') for word in text.splitlines()]
    space = font.size(' ')[0]  # The width of a space.
    max_width, max_height = surface.get_size()
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = pos[0]  # Reset the x.
                y += word_height  # Start on new row.
            surface.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]  # Reset the x.
        y += word_height  # Start on new row.

initSOI()
createBG()
main()
