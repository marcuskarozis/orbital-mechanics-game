from math import *

WIDTH, HEIGHT = 1000, 800
FPS = 60


pause = False
DEBUG = False

# Star Variables
starNum = 80
star_Lum_Min = 20
star_Lum_Max = 255
star_Speed_Min = 3
star_Speed_Max = 10


GRAVITY = 0.0003 # UNIVERSAL GRAVITY CONSTANT
INFLUENCE_SPHERE_CONSTANT = 1
FIRE_POWER_CONSTANT = 0.01

TRAIL_LENGNTH = 101

#define some commonly used colours
WHITE = (255, 255, 255)
LIGHTGREY = (192, 192, 192)
DARKGREY = (128, 128, 128)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BRICKRED = (178, 34, 34)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
MAGENTA = (255, 0, 255)
CYAN = (0, 255, 255)

class Star:
    def __init__(self,ID, x, y, lum, blnkSpeed, blnkDir):
        self.ID = ID
        self.x = x
        self.y = y
        self.lum = lum
        self.blnkSpeed = blnkSpeed
        self.blnkDir = blnkDir

class Body:
    def __init__(self, pos, vel, radius, mass, parentID, static, colour):
        self.pos = pos
        self.vel = vel
        self.radius = radius
        self.mass = mass
        self.parentID = parentID
        self.static = static
        self.colour = colour
        self.rSOI = 0
        self.Orbits = 0
        #self.posHistory = [pos]*TRAIL_LENGNTH
        self.burnComplete = True

    def update(self):
        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]
        #self.posHistory = self.posHistory[-1:] + self.posHistory[:-1]
        #self.posHistory[0] = self.pos
        


    def calcRSOI(self, v, r, M, m):
        if self.static == True:
            self.rSOI = 10000
        else:
            u = GRAVITY*M**3/(M+m)**2

            a = (u*r)/(2*u-r*v**2)
            self.rSOI = a*(m/M)**(2/5)


class Craft:
    def __init__(self, pos, vel, radius, mass, parentID, static, colour):
        self.pos = pos
        self.vel = vel
        self.radius = radius
        self.mass = mass
        self.parentID = parentID
        self.static = static
        self.colour = colour
        self.velBest = 0
        #self.posHistory = [pos]*TRAIL_LENGNTH
        self.burnComplete = False

    def update(self):
        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]
        #self.posHistory = self.posHistory[-1:] + self.posHistory[:-1]
        #self.posHistory[0] = self.pos
        
def bounceNum(num, delta, bounceMax, bounceMin, bounceDir):
    if bounceDir:
        out = num + delta
        if out > bounceMax:
            out = bounceMax - (out-bounceMax)
            bounceDir = False
    else:
        out = num - delta
        if out < bounceMin:
            out = bounceMin + (bounceMin-out)
            bounceDir = True
    return [out, bounceDir]


def getBDist(origin, target, mode="body"):
    if mode == "body":
        return sqrt((origin.pos[0]-target.pos[0])**2 + (origin.pos[1]-target.pos[1])**2)
    elif mode == "pos":
        return sqrt((origin[0]-target[0])**2 + (origin[1]-target[1])**2)

